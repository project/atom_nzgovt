<?php
/**
 * @file views-view-row-rss.tpl.php
 * Default view template to display a item in an RSS feed.
 *
 * @ingroup views_templates
 */
?>
  <entry>
    <title><?php print $title; ?></title>
    <link rel="alternate" type="text/html" href="<?php print $link; ?>" />
    <content type="xhtml"><div xmlns="http://www.w3.org/1999/xhtml"><?php print $content; ?></div></content>
    <?php foreach($attachements as $index => $attachement) : ?>
      <link rel="enclosure" title="<?php print $attachement['description']; ?>" type="<?php print $attachement['filemime']; ?>" href="<?php print file_create_url($attachement['uri']); ?>" length="<?php print $attachement['filesize']; ?>" />
    <?php endforeach; ?>
    <?php foreach($category as $index => $cat) : ?>
      <category term="<?php print $cat; ?>" scheme="http://www.e.govt.nz/standards/nz/2009-03-01#information-type" />
    <?php endforeach; ?>
    <?php foreach($terms as $index => $term) : ?>
      <category term="<?php print $term; ?>" />
    <?php endforeach; ?>
    <?php print $item_elements; ?>
  </entry>
